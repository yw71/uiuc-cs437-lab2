var nodeConsole = require("console");
var myConsole = new nodeConsole.Console(process.stdout, process.stderr);

document.onkeydown = updateKey;
document.onkeyup = resetKey;

var server_port = 65432;
var server_addr = "192.168.1.87"; // the IP address of your Raspberry PI

function client(input) {
  const net = require("net");

  const client = net.createConnection(
    { port: server_port, host: server_addr },
    () => {
      // 'connect' listener.
      myConsole.log("connected to server!");
      // send the message
      client.write(input);
    }
  );

  // get the data from the server
  client.on("data", (data) => {
    const sensor_data = JSON.parse(data.toString());
    myConsole.log(sensor_data);

    document.getElementById("ultrasonics").innerText = sensor_data["ultra"];
    document.getElementById("battery").innerText = sensor_data["battery"];
    document.getElementById("temperature").innerText = sensor_data["temp"];

    client.end();
    client.destroy();
  });

  client.on("end", () => {
    myConsole.log("disconnected from server");
  });
}

// for detecting which key is been pressed w,a,s,d
function updateKey(e) {
  e = e || window.event;

  if (e.keyCode == "87") {
    // up (w)
    document.getElementById("upArrow").style.color = "green";
    send_data("up");
  } else if (e.keyCode == "83") {
    // down (s)
    document.getElementById("downArrow").style.color = "green";
    send_data("down");
  } else if (e.keyCode == "65") {
    // left (a)
    document.getElementById("leftArrow").style.color = "green";
    send_data("left");
  } else if (e.keyCode == "68") {
    // right (d)
    document.getElementById("rightArrow").style.color = "green";
    send_data("right");
  } else if (e.keyCode == "81") {
    // quit (q)
    document.getElementById("stop").style.color = "green";
    send_data("stop");
  }
}

// reset the key to the start state
function resetKey(e) {
  e = e || window.event;
  document.getElementById("upArrow").style.color = "grey";
  document.getElementById("downArrow").style.color = "grey";
  document.getElementById("leftArrow").style.color = "grey";
  document.getElementById("rightArrow").style.color = "grey";
  document.getElementById("stop").style.color = "grey";
}

function update_data() {
  setInterval(function () {
    client("sensor");
  }, 500);
}

function send_data(text) {
  client(text);
}
