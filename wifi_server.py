import socket
import json
import picar_4wd as fc
from picar_4wd.utils import power_read, cpu_temperature

HOST = "192.168.1.87" # IP address of your Raspberry PI
PORT = 65432          # Port to listen on (non-privileged ports are > 1023)
POWER = 10

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()

    try:
        while 1:
            client, clientInfo = s.accept()
            data = client.recv(1024) 

            if data == b"sensor":
                m = {"ultra": fc.get_distance_at(0), 
                    "battery": power_read(), 
                    "temp": cpu_temperature()}
                print("dumping into json: ")
                response = json.dumps(m)
                print("printing response: ")
                print(response)
                client.sendall(bytes(response,encoding="utf-8"))

            elif data == b"up":
                fc.forward(POWER)
            elif data==b"left":
                fc.turn_left(POWER)
            elif data==b"down":
                fc.backward(POWER)
            elif data==b"right":
                fc.turn_right(POWER)
            elif data==b"stop":
                fc.stop()

    except: 
        print("Closing socket")
        client.close()
        s.close()    